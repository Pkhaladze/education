<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\Level;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SubjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::paginate(15);
        return view('administrator.subjects.index')->with(compact('subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacher_model = new Teacher();
        $teachers = $teacher_model->getTeachersById();
        
        $level_model = new Level();
        $levels = $level_model->getLevelsById();

        return view('administrator.subjects.create', compact('levels', 'teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:20'
        ]);
        $subjects = new Subject;
        //dd($subjects);
        $subjects->name = $request->name;
        $subjects->teacher_id = $request->teacher_id;
        $subjects->level_id = $request->level_id;
        $subjects->save();
        $subjects = Subject::paginate(15);
        return view('administrator.subjects.index')->with(compact('subjects'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher_model = new Teacher();
        $teachers = $teacher_model->getTeachersById();
        
        $level_model = new Level();
        $levels = $level_model->getLevelsById();

        $subject = Subject::find($id);
        return view('administrator.subjects.edit', compact('levels', 'teachers', 'subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:20',
            'teacher_id' => 'required|max:5',
            'level_id' => 'required|max:5',
        ]);
        //dd($request);
        $subjects = new Subject;
        //dd($subjects);
        $subjects->name = $request->name;
        $subjects->teacher_id = $request->teacher_id;
        $subjects->level_id = $request->level_id;
        $subjects->save();
        $subjects = Subject::paginate(15);
        return view('administrator.subjects.index')->with(compact('subjects'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
