<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->delete();
        $insertArray = [
        	[
             'name'=>'mathematics', 'teacher_id'=>'1', 'level_id'=>'1'
            ],
            [
             'name'=>'physics', 'teacher_id'=>'3', 'level_id'=>'2'
            ], 
            [
             'name'=>'chemistry', 'teacher_id'=>'2', 'level_id'=>'3'
            ],
            [
             'name'=>'history', 'teacher_id'=>'6', 'level_id'=>'4'
            ],
            [
             'name'=>'geography', 'teacher_id'=>'7', 'level_id'=>'5'
            ],
        ];
        DB::table('subjects')->insert($insertArray);
    }
}
