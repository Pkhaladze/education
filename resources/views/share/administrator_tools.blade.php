<div class="col-xs-6 col-sm-3 sidebar-offcanvas">
  <div class="list-group">
    <a href="{{ url('administrator/teachers') }}" class="list-group-item">Teachers</a>
    <a href="{{ url('administrator/students') }}" class="list-group-item">Students</a>
    <a href="{{ url('administrator/subjects') }}" class="list-group-item">Subjects</a>
    <a href="{{ url('administrator/levels') }}" class="list-group-item">Classes</a>
    <a href="{{ url('administrator/semesters') }}" class="list-group-item">Semesters</a>
  </div>
</div><!--/.sidebar-offcanvas-->