@extends('layouts.master')
@section('content')
<div class="container">
	@include('share.administrator_tools')
	<form method="POST" action="{{url('administrator/teachers/'.$teacher->id)}}">
		<div class="form-horizontal col-xs-18 col-sm-9">
			<fieldset>
				<legend>Edit teacher</legend>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label">First Name</label>  
				  <div class="col-md-6">
				 	 <input  name="first_name" value="{{old('first_name',$teacher->first_name)}}" type="text" placeholder="first_name" class="form-control input-md">  
				  </div>
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label">Last Name</label>  
				  <div class="col-md-6">
				 	 <input  name="last_name" value="{{old('last_name',$teacher->last_name)}}" type="text" placeholder="last_name" class="form-control input-md">  
				  </div>
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label">Personal Number</label>  
				  <div class="col-md-6">
				 	 <input  name="personal_id" value="{{old('personal_id',$teacher->personal_id)}}" type="text" placeholder="personal_id" class="form-control input-md">  
				  </div>
				</div>	
				<!-- Sabject input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Subjects</label>  
                  <div class="col-md-6">
                     <select class="form-control input-md" id="sabjects_select" name="subject_id">
                       <option value=""></option>
                        @foreach ($subjects as $id => $subject)
                           <option
                              @if(isset($teacher->subject->id) && $teacher->subject->id==$id)selected="selected"  
                                  @endif value="2">{{$subject}}
                            </option>
                       @endforeach
                       <option value=""></option>
                     </select>
                  </div>
                </div>
				<!-- Button -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="singlebutton"></label>
				  <div class="col-md-4">
				    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Update</button>
				    <input type="hidden" value="put" name="_method">
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
				  </div>
				</div>
			</fieldset>
		</div>
	</form>
</div>
@endsection