@extends('layouts.master')
@section('content')
<div class="container">
	@include('share.administrator_tools')
	<div  class="table-responsive">
	<a class="btn btn-primary pull-right" href="{{url('administrator/semesters/create')}}" role="button">Add</a>
		<table class="table table-striped"> 
			<thead> 
				<tr> 
					<th>ID</th> 
					<th>Semester</th> 
					<th>Start Date</th> 
					<th>Finish Date</th> 
					<th>Active</th> 
					<th colspan="2">Actions</th> 
				</tr>
			</thead>
			<tbody>
				@foreach($semesters as $semester)
					<tr> 
						<td>{{$semester->id}}</td> 
						<td>@if($semester->semester_type==1) springtime @else autumn @endif </td>
						<td>{{$semester->start_date}}</td> 
						<td>{{$semester->finish_date}}</td> 
						<td>{{$semester->active}}</td>
						<td>
						    <a href="{{url('/administrator/semesters/'.$semester->id.'/edit')}}">
						    	<i class="glyphicon glyphicon-edit"></i>
						    </a>
						</td>
						<td>
					    	<form method="POST" action="{{url('administrator/semesters/'.$semester->id)}}">
								<button type="submit" style="background: transparent; border: 0;" id="singlebutton" name="singlebutton" ><i class="glyphicon glyphicon-remove-circle"></i></button>
								<input type="hidden" value="delete" name="_method">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
							</form>
						</td> 
					</tr>
				@endforeach
			</tbody>
		</table>
		<div class="text-center">
			{!! $semesters->render() !!}
		</div>
	</div>
</div>
@endsection