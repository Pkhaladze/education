<?php

use Illuminate\Database\Seeder;

class SemestersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('semesters')->delete();
        $insertArray = [
        	[
            'semester_type' => '1',
            'start_date' =>'2010-09-01',
            'finish_date' => '2010-12-20',
            'active' => '0'
            ],
        	[
            'semester_type' => '2',
            'start_date' =>'2011-01-15',
            'finish_date' => '2011-05-31',
            'active' => '0'
            ],
            [
            'semester_type' => '1',
            'start_date' =>'2011-09-01',
            'finish_date' => '2011-12-20',
            'active' => '0'
            ],
        	[
            'semester_type' => '2',
            'start_date' =>'2012-01-15',
            'finish_date' => '2012-05-31',
            'active' => '0'
            ],
                    	[
            'semester_type' => '1',
            'start_date' =>'2012-09-01',
            'finish_date' => '2012-12-20',
            'active' => '0'
            ],
        	[
            'semester_type' => '2',
            'start_date' =>'2013-01-15',
            'finish_date' => '2013-05-31',
            'active' => '0'
            ],
                    	[
            'semester_type' => '1',
            'start_date' =>'2013-09-01',
            'finish_date' => '2013-12-20',
            'active' => '0'
            ],
        	[
            'semester_type' => '2',
            'start_date' =>'2014-01-15',
            'finish_date' => '2014-05-31',
            'active' => '0'
            ],
                    	[
            'semester_type' => '1',
            'start_date' =>'2014-09-01',
            'finish_date' => '2014-12-20',
            'active' => '0'
            ],
        	[
            'semester_type' => '2',
            'start_date' =>'2015-01-15',
            'finish_date' => '2015-05-31',
            'active' => '0'
            ],
                    	[
            'semester_type' => '1',
            'start_date' =>'2015-09-01',
            'finish_date' => '2015-12-20',
            'active' => '0'
            ],
        	[
            'semester_type' => '2',
            'start_date' =>'2016-01-15',
            'finish_date' => '2016-05-31',
            'active' => '1'
            ],
                    	[
            'semester_type' => '1',
            'start_date' =>'2017-09-01',
            'finish_date' => '2017-12-20',
            'active' => '0'
            ],
        	[
            'semester_type' => '2',
            'start_date' =>'2018-01-15',
            'finish_date' => '2018-05-31',
            'active' => '0'
            ],
            
        ];
        DB::table('semesters')->insert($insertArray);

    }
}