<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Models\Teacher;
use App\Models\Subject;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TeachersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $teachers = Teacher::paginate(15);
        return view('administrator.teachers.index')->with(compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subject_model = new Subject();
        $subjects = $subject_model->getSubjectsById();
        return view('administrator/teachers/create')->with(compact('subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:20',
            'last_name' => 'required|max:20',
            'personal_id' => 'required|max:11',
        ]);
        $teachers = new Teacher;
        $teachers->first_name = $request->first_name;
        $teachers->last_name = $request->last_name;
        $teachers->personal_id = $request->personal_id;
        $teachers->subject_id = $request->subject_id;
        $teachers->save();
        return view('administrator.teachers.index')->with(compact('teachers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::find($id);

        $subject_model = new Subject();
        $subjects = $subject_model->getSubjectsById();
        return view('administrator/teachers/edit')->with(compact('teacher' , 'subjects'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|max:20',
            'last_name' => 'required|max:20',
            'personal_id' => 'required|max:11',
        ]);
        $teachers = Teacher::find($id);
        $teachers->first_name = $request->first_name;
        $teachers->last_name = $request->last_name;
        $teachers->personal_id = $request->personal_id;
        $teachers->subject_id = $request->subject_id;
        $teachers->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Teacher::find($id);
        Teacher::where('id', $id)
            ->delete();
        return redirect()->back();

    }
}
