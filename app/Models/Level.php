<?php

namespace App\Models;
use App\Models\Level;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'create_year', 'quota',
    ];

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

        public function subject()
    {
        return $this->hasOne('App\Models\Subject');
    }

    public function getLevelsById(){
        $levels = Level::all();

        $return_array = [];
        foreach ($levels as $level) {
            $return_array[$level->id] = $level->name;
        }

        return $return_array;
    }
}
