<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\Level;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::paginate(15);
        return view('administrator.students.index')->with(compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrator/students/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required|max:20',
            'last_name' => 'required|max:20',
            'personal_id' => 'required|max:11',
            'birthday' => 'required',
            'enter_date' => 'required',
        ]);
        $students = new Student;
        $students->first_name = $request->first_name;
        $students->last_name = $request->last_name;
        $students->level_id = $request->level_id;
        $students->personal_id = $request->personal_id;
        $students->birthday = $request->birthday;
        $students->enter_date = $request->enter_date;
        $students->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = student::find($id);

        $level_model = new Level();
        $levels = $level_model->getLevelsById();
        return view('administrator/students/edit')->with(compact('student', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|max:20',
            'last_name' => 'required|max:20',
            'personal_id' => 'required|max:11',
            'birthday' => 'required',
            'enter_date' => 'required',
        ]);
        $students = Student::find($id);
        $students->first_name = $request->first_name;
        $students->last_name = $request->last_name;
        $students->level_id = $request->level_id;
        $students->personal_id = $request->personal_id;
        $students->birthday = $request->birthday;
        $students->enter_date = $request->enter_date;

        $students->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::find($id);
        Student::where('id', $id)
            ->delete();
        return redirect()->back();

    }
}
