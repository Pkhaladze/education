<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function level()
    {
        return $this->hasOne('App\Models\Level');
    }
    public function subject()
    {
        return $this->belongsTo('App\Models\Subject');
    }
    public function getTeachersById(){
        $teachers = Teacher::all();
        $return_array = [];
        foreach ($teachers as $teacher) {
            $return_array[$teacher->id] = $teacher->first_name . ' ' . $teacher->last_name;
        }

        return $return_array;
    }
}
