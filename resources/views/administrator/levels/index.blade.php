@extends('layouts.master')
@section('content')
<div class="container">
    @include('share.administrator_tools')
    <div  class="table-responsive">
    <a class="btn btn-primary pull-right"  href="{{url('administrator/levels/create')}}" role="button">Add</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Create Year</th>
                    <th>Quota</th>
                    <th>Teacher</th>
                    <th>Schedule</th>
                    <th colspan="2">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($levels as $level)
                    <tr>
                        <th scope="row">{{$level->id}}</th>
                        <td>{{$level->name}}</td>
                        <td>{{$level->create_year}}</td>
                        <td>{{$level->quota}}</td>
                        <td>{{$level->teacher->first_name . ' ' . $level->teacher->last_name}}</td>
                        <td><a href="{{url('/administrator/levels/schedule/'.$level->id)}}">
                                Schedule
                            </a></td>
                        <td>
                            <a href="{{url('/administrator/levels/'.$level->id.'/edit')}}">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </td>
                        <td>
                            <form method="POST" action="{{url('administrator/levels/'.$level->id)}}">
                                <button type="submit" style="background: transparent; border: 0;" id="singlebutton" name="singlebutton" ><i class="glyphicon glyphicon-remove-circle"></i></button>
                                <input type="hidden" value="delete" name="_method">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {!! $levels->render() !!}
        </div>
    </div>
</div>
@endsection
