@extends('layouts.master')
@section('content')
<div class="container">
	@include('share.administrator_tools')
	<div  class="table-responsive">
	<a class="btn btn-primary pull-right" href="{{url('administrator/students/create')}}" role="button">Add</a>
		<table class="table table-striped"> 
			<thead> 
				<tr> 
					<th>ID</th> 
					<th>First Name</th> 
					<th>Last Name</th>
					<th>Class</th>
					<th>Personal Number</th> 
					<th>BirthDay</th> 
					<th>Enter Date</th>
					<th colspan="2">Actions</th> 
				</tr>
			</thead>
			<tbody>
				@foreach($students as $student)
					<tr> 
						<th scope="row">{{$student->id}}</th> 
						<td>{{$student->first_name}}</td> 
						<td>{{$student->last_name}}</td> 
						<td>
							@if(isset($student->level->id))
								{{$student->level->name}}
							@endif	
						</td>  
						<td>{{$student->personal_id}}</td> 
						<td>{{$student->birthday}}</td> 
						<td>{{$student->enter_date}}</td>  
						<td>
						    <a href="{{url('/administrator/students/'.$student->id.'/edit')}}">
						    	<i class="glyphicon glyphicon-edit"></i>
						    </a>
						</td>
						<td>
					    	<form method="POST" action="{{url('administrator/students/'.$student->id)}}">
								<button type="submit" style="background: transparent; border: 0;" id="singlebutton" name="singlebutton" ><i class="glyphicon glyphicon-remove-circle"></i></button>
								<input type="hidden" value="delete" name="_method">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
							</form>
						</td> 
					</tr>
				@endforeach
			</tbody>
		</table>
		<div class="text-center">
			{!! $students->render() !!}
		</div>
	</div>
</div>
@endsection