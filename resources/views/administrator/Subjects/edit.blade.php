@extends('layouts.master')
@section('content')
<div class="container">
    @include('share.administrator_tools')
    <form method="POST" action="{{url('administrator/subjects/'.$subject->id)}}">
        <div class="form-horizontal col-xs-18 col-sm-9">
            <fieldset>
                <legend>Edit subject</legend>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Name</label>  
                  <div class="col-md-6">
                     <input  name="name" value="{{old('name',$subject->name)}}" type="text" placeholder="Name" class="form-control input-md">  
                  </div>
                </div>
                <!-- teachers_select -->
                <div class="form-group">
                  <label class="col-md-4 control-label">Teacher</label>  
                  <div class="col-md-6">
                     <select class="form-control input-md" id="teachers_select" name="teacher_id">
                       <option value=""></option>
                        @foreach ($teachers as $id => $teacher)
                           <option
                              @if(isset($subject->teacher->id) && $subject->teacher->id==$id)selected="selected"  
                                  @endif value="{{$id}}">{{$teacher}}
                            </option>
                       @endforeach
                       <option value=""></option>
                     </select>
                  </div>
                </div>
                <!-- classes-select -->
                <div class="form-group">
                  <label class="col-md-4 control-label">Classes</label>  
                  <div class="col-md-6">
                     <select class="form-control input-md" id="levels_select" name="level_id">
                        <option></option>
                        @foreach ($levels as $id => $level)
                           <option 
                              @if($id==$subject->level->id)selected="selected"  
                                  @endif value="{{$id}}">{{$level}}
                            </option>
                       @endforeach
                     </select>
                  </div>
                </div>
                <!-- Button -->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="singlebutton"></label>
                  <div class="col-md-4">
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Update</button>
                    <input type="hidden" value="put" name="_method">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </div>
                </div>
            </fieldset>
        </div>
    </form>
</div>
<script>
  $(document).ready(function(){
      $('#teachers_select').select2({
        placeholder: "Select teacher"
      });
  });
</script>
<script>
  $(document).ready(function(){
      $('#levels_select').select2({
          placeholder: "Select level",
          allowClear: true,
      });
  });
</script>
@endsection