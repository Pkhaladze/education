@extends('layouts.master')
@section('content')
<div class="container">
	@include('share.administrator_tools')
	<div  class="table-responsive">
	<a class="btn btn-primary pull-right" href="{{url('administrator/teachers/create')}}" role="button">Add</a>
		<table class="table table-striped"> 
			<thead> 
				<tr> 
					<th>ID</th> 
					<th>First Name</th> 
					<th>Last Name</th> 
					<th>Personal Number</th>
					<th>Subjects</th>
					<th colspan="2">Actions</th> 
				</tr>
			</thead>
			<tbody>
				@foreach($teachers as $teacher)
					<tr> 
						<th scope="row">{{$teacher->id}}</th> 
						<td>{{$teacher->first_name}}</td> 
						<td>{{$teacher->last_name}}</td> 
						<td>{{$teacher->personal_id}}</td> 
						<td>
							@if(isset($teacher->subject->id))
								{{$teacher->subject->name}}
							@endif	
						</td>  
						<td>
						    <a href="{{url('/administrator/teachers/'.$teacher->id.'/edit')}}">
						    	<i class="glyphicon glyphicon-edit"></i>
						    </a>
						</td>
						<td>
					    	<form method="POST" action="{{url('administrator/teachers/'.$teacher->id)}}">
								<button type="submit" style="background: transparent; border: 0;" id="singlebutton" name="singlebutton" ><i class="glyphicon glyphicon-remove-circle"></i></button>
								<input type="hidden" value="delete" name="_method">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
							</form>
						</td> 
					</tr>
				@endforeach
			</tbody>
		</table>
		<div class="text-center">
			{!! $teachers->render() !!}
		</div>
	</div>
</div>
@endsection