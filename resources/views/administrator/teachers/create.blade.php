@extends('layouts.master')
@section('content')
<div class="container">
	@include('share.administrator_tools')
	<form method="POST" action="{{url('administrator/teachers')}}">
		<div class="form-horizontal col-xs-18 col-sm-9">
			<fieldset>
				<legend>Create teacher</legend>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label">First Name</label>  
				  <div class="col-md-6">
				 	 <input  name="first_name" value="" type="text" placeholder="first_name" class="form-control input-md">  
				  </div>
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label">Last Name</label>  
				  <div class="col-md-6">
				 	 <input  name="last_name" value="" type="text" placeholder="last_name" class="form-control input-md">  
				  </div>
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label">Personal Number</label>  
				  <div class="col-md-6">
				 	 <input  name="personal_id" value="" type="text" placeholder="personal_id" class="form-control input-md">  
				  </div>
				</div>
				<!-- Button -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="singlebutton"></label>
				  <div class="col-md-4">
				    <button id="singlebutton" name="singlebutton" class="btn btn-primary">ADD</button>
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
				  </div>
				</div>
			</fieldset>
		</div>
	</form>
</div>
@endsection