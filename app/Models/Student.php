<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function level()
    {
        return $this->belongsTo('App\Models\Level');
    }
    public function getLevelsById(){
        $levels = Level::all();
        $return_array = [];
        foreach ($levels as $level) {
            $return_array[$level->id] = $level->name;
        }

        return $return_array;
    }
}
