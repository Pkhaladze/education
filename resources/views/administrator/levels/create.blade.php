@extends('layouts.master')
@section('content')
<div class="container">
	@include('share.administrator_tools')
	<form method="POST" action="{{url('administrator/levels')}}">
		<div class="form-horizontal col-xs-18 col-sm-9">
			<fieldset>
				<legend>Create Class</legend>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label">Name</label>  
				  <div class="col-md-6">
				 	 <input  name="name" value="" type="text" placeholder="Name" class="form-control input-md">  
				  </div>
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label">Create Year</label>  
				  <div class="col-md-6">
				 	 <input  name="create_year" value="" type="text" placeholder="yyyy-mm-dd" class="form-control input-md">  
				  </div>
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label">Quota</label>  
				  <div class="col-md-6">
				 	 <input  name="quota" value="" type="text" placeholder="Quota" class="form-control input-md">  
				  </div>
				</div>
                <div class="form-group">
                  <label class="col-md-4 control-label">Teacher</label>  
                  <div class="col-md-6">
                     <select class="form-control input-md" id="teachers_select" name="teacher_id">
                       <option></option>
                       @foreach ($teachers as $id => $teacher)
                           <option value="{{$id}}">{{$teacher}}</option>}
                       @endforeach
                     </select>
                  </div>
                </div>
				<!-- Button -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="singlebutton"></label>
				  <div class="col-md-4">
				    <button id="singlebutton" name="singlebutton" class="btn btn-primary">ADD</button>
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
				  </div>
				</div>
			</fieldset>
		</div>
	</form>
</div>
@endsection