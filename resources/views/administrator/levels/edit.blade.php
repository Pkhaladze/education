@extends('layouts.master')
@section('content')
<div class="container">
    @include('share.administrator_tools')
    <form method="POST" action="{{url('administrator/levels/'.$level->id)}}">
        <div class="form-horizontal col-xs-18 col-sm-9">
            <fieldset>
                <legend>Edit level</legend>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Name</label>
                  <div class="col-md-6">
                     <input  name="name" value="{{old('name',$level->name)}}" type="text" placeholder="Name" class="form-control input-md">
                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Create Year</label>
                  <div class="col-md-6">
                     <input  name="create_year" value="{{old('create_year',$level->create_year)}}" type="text" placeholder="Create year" class="form-control input-md">
                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Quota</label>
                  <div class="col-md-6">
                     <input  name="quota" value="{{old('quota',$level->quota)}}" type="text" placeholder="Quota" class="form-control input-md">
                  </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label">Teacher</label>
                  <div class="col-md-6">
                     <select id="teachers_select" name="teacher_id" style="width: 100%">
                       @foreach ($teachers as $id => $teacher)
                           <option @if($id==$level->teacher->id)selected="selected"  @endif value="{{$id}}">{{$teacher}}</option>
                       @endforeach
                     </select>
                  </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="singlebutton"></label>
                  <div class="col-md-4">
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Update</button>
                    <input type="hidden" value="put" name="_method">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </div>
                </div>
            </fieldset>
        </div>
    </form>
</div>
<script>
  $(document).ready(function(){
      $('#teachers_select').select2({
        placeholder: "Select teacher"
      });
  });
</script>
@endsection
