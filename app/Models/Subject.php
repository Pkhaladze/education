<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }
     public function level()
    {
        return $this->belongsTo('App\Models\Level');
    }
    public function getSubjectsById(){
        $subjects = Subject::all();

        $return_array = [];
        foreach ($subjects as $subject) {
            $return_array[$subject->id] = $subject->name;
        }

        return $return_array;
    }
}
