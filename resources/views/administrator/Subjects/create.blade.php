@extends('layouts.master')
@section('content')
<div class="container">
	@include('share.administrator_tools')
	<form method="POST" action="{{url('administrator/subjects')}}">
		<div class="form-horizontal col-xs-18 col-sm-9">
			<fieldset>
				<legend>Create subject</legend>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label">Name</label>  
				  <div class="col-md-6">
				 	 <input  name="name" value="" type="text" placeholder="name" class="form-control input-md">  
				  </div>
				</div>
           <!-- teachers_select -->
          <div class="form-group">
            <label class="col-md-4 control-label">Teacher</label>  
            <div class="col-md-6">
               <select class="form-control input-md" id="teachers_select" name="teacher_id">
                 <option></option>
                 @foreach ($teachers as $id => $teacher)
                     <option value="{{$id}}">{{$teacher}}</option>
                 @endforeach
               </select>
            </div>
          </div>
          <!-- classes-select -->
          <div class="form-group">
            <label class="col-md-4 control-label">Classes</label>  
            <div class="col-md-6">
               <select class="form-control input-md" id="levels_select" name="level_id">
                 <option></option>
                 @foreach ($levels as $id => $level)
                     <option value="{{$id}}">{{$level}}</option>
                 @endforeach
               </select>
            </div>
          </div>
				<!-- Button -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="singlebutton"></label>
				  <div class="col-md-4">
				    <button id="singlebutton" name="singlebutton" class="btn btn-primary">ADD</button>
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
				  </div>
				</div>
			</fieldset>
		</div>
	</form>
</div>
<script>
  $(document).ready(function(){
      $('#teachers_select').select2({
        placeholder: "Select teacher",
        allowClear: true,
      });
  });
    $(document).ready(function(){
      $('#levels_select').select2({
        placeholder: "Select Classes",
        allowClear: true,
      });
  });
</script>
@endsection