<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/', 'AdministratorController@index');
Route::get('/home', 'MainManuController@home');
Route::get('/about', 'MainManuController@about');
Route::get('/news', 'MainManuController@news');
Route::get('/contact', 'MainManuController@contact');

Route::group(['namespace' => 'Administrator','prefix' => 'administrator'], function () {

    Route::resource('teachers', 'TeachersController');
    Route::resource('students', 'StudentsController');
    Route::resource('levels', 'LevelsController');
    //Route::resource('levels/schedule/{id}', 'LevelsController@schedule');
    Route::resource('subjects', 'SubjectsController');
    Route::resource('semesters', 'SemestersController');
});