@extends('layouts.master')
@section('content')
<div class="container">
	@include('share.administrator_tools')
	<div  class="table-responsive">
	<a class="btn btn-primary pull-right" href="{{url('administrator/subjects/create')}}" role="button">Add</a>
		<table class="table table-striped"> 
			<thead> 
				<tr> 
					<th>ID</th> 
					<th>Name</th> 
					<th>Teacher</th> 
					<th>Classes</th> 
					<th colspan="2">Actions</th> 
				</tr>
			</thead>
			<tbody>
				@foreach($subjects as $subject)
					<tr> 
						<th scope="row">{{$subject->id}}</th> 
						<td>{{$subject->name}}</td> 
						<td>
							@if(isset($subject->teacher->id))
								{{$subject->teacher->first_name . ' ' . $subject->teacher->last_name}}
							@endif	
						</td> 
						
						<td>
							@if(isset($subject->level->id))
								{{$subject->level->name}}
							@endif
						<td>
						    <a href="{{url('/administrator/subjects/'.$subject->id.'/edit')}}">
						    	<i class="glyphicon glyphicon-edit"></i>
						    </a>
						</td>
						<td>
					    	<form method="POST" action="{{url('administrator/subjects/'.$subject->id)}}">
								<button type="submit" style="background: transparent; border: 0;" id="singlebutton" name="singlebutton" ><i class="glyphicon glyphicon-remove-circle"></i></button>
								<input type="hidden" value="delete" name="_method">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
							</form>
						</td> 
					</tr>
				@endforeach
			</tbody>
		</table>
		<div class="text-center">
			{!! $subjects->render() !!}
		</div>
	</div>
</div>
@endsection