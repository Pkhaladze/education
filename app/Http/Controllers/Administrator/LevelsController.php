<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Level;
use App\Models\Teacher;
use Illuminate\Http\Request;

class LevelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levels = Level::paginate(15);
        return view('administrator.levels.index')->with(compact('levels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacher_model = new Teacher();
        $teachers = $teacher_model->getTeachersById();
        return view('administrator.levels.create', compact('level', 'teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:20',
            'create_year' => 'required|max:20',
            'quota' => 'required|max:3',
            'teacher_id' => 'required|max:5',
        ]);
        $levels = new Level;
        $levels->name = $request->name;
        $levels->create_year = $request->create_year;
        $levels->quota = $request->quota;
        $levels->teacher_id = $request->teacher_id;
        $levels->save();
        $levels = Level::paginate(15);
        return view('administrator.levels.index')->with(compact('levels'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher_model = new Teacher();
        $teachers = $teacher_model->getTeachersById();
        $level = Level::find($id);
        return view('administrator.levels.edit', compact('level', 'teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|max:20',
            'create_year' => 'required|max:20',
            'quota' => 'required|max:3',
            'teacher_id' => 'required|max:5',
        ]);
        //dd($request);
        $levels = Level::find($id);
        $levels->name = $request->name;
        $levels->create_year = $request->create_year;
        $levels->quota = $request->quota;
        $levels->teacher_id = $request->teacher_id;

        $levels->save();
        $levels = Level::paginate(15);
        return view('administrator.levels.index')->with(compact('levels'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function schedule($id)
    {
        
        return 'shchedule - ' . $id;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Level::find($id);
        Level::where('id', $id)
            ->delete();
        $levels = Level::paginate(15);
        return view('administrator.levels.index')->with(compact('levels'));
    }
}
