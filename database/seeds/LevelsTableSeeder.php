<?php

use Illuminate\Database\Seeder;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->delete();
        $insertArray = [
        	[
            'name' => '1ა',
            'create_year' => '2015-07-03',
            'quota' => 45,
            'teacher_id' => 1,
            ],
            [
            'name' => '1ბ',
            'create_year' => '2015-07-03',
            'quota' => 20,
            'teacher_id' => 2,
            ], 
            [
            'name' => '3ა',
            'create_year' => '2014-07-03',
            'quota' => 25,
            'teacher_id' => 3,
            ], 
            [
            'name' => '4ბ',
            'create_year' => '2014-07-03',
            'quota' => 30,
            'teacher_id' => 4,
            ],
            [
            'name' => '5ბ',
            'create_year' => '2014-07-03',
            'quota' => 30,
            'teacher_id' => 5,
            ], 
            [
            'name' => '6ბ',
            'create_year' => '2014-07-03',
            'quota' => 56,
            'teacher_id' => 6,
            ], 
            [
            'name' => '7ბ',
            'create_year' => '2014-07-03',
            'quota' => 35,
            'teacher_id' => 7,
            ],   
        ];
        DB::table('levels')->insert($insertArray);
    }
}